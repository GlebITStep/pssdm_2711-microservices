var express = require('express');
var router = express.Router();
let mongo = require('mongodb');
let MongoClient = mongo.MongoClient;

const url = 'mongodb://localhost:27017';
const dbName = 'myshopdb';


var data = [
    { id: 1, title: 'One', price: 1 },
    { id: 2, title: 'Two', price: 2 },
    { id: 3, title: 'Three', price: 3 },
];

// GET /products
/**
 * @swagger
 * /products:
 *    get:
 *      description: Return all products    
 */
router.get('/', async (req, res) => {
    const client = new MongoClient(url, { useUnifiedTopology: true, useNewUrlParser: true });
    let dbClient = await client.connect();
    let db = dbClient.db(dbName);
    let collection = db.collection('products');

    let result = await collection.find({}).toArray();

    res.json(result);
});

// GET /products/5
router.get('/:id', async (req, res) => {
    let id = req.params.id;
    if (id) {
        const client = new MongoClient(url, { useUnifiedTopology: true, useNewUrlParser: true });
        let dbClient = await client.connect();
        let db = dbClient.db(dbName);
        let collection = db.collection('products');
        let data = await collection.findOne({ _id: mongo.ObjectId(id) });
        res.json(data);
    } else {
        res.statusCode = 404;
        res.statusMessage = 'Product not found!';
        let error = {
            code: res.statusCode,
            message: res.statusMessage
        };
        res.json(error);
    }
});

// POST /products
router.post('/', (req, res) => {
    let item = req.body;
    if (item.title && item.price) {
        item.id = data[data.length - 1].id + 1;
        data.push(item);
        res.json(item); 
    } else {
        res.statusCode = 400; //Bad request
        res.statusMessage = 'Bad request!';
        let error = {
            code: res.statusCode,
            message: res.statusMessage
        };
        res.json(error);
    }
});

// PUT /products
router.put('/', (req, res) => {
    let item = req.body;
    let index = data.findIndex(x => x.id == item.id);
    if (index != -1) {
        data[index] = item;
        res.json(item);        
    } else {
        res.statusCode = 404; //Bad request
        res.statusMessage = 'Product not found!';
        let error = {
            code: res.statusCode,
            message: res.statusMessage
        };
        res.json(error);
    }
});

// DELETE /product/5
router.delete('/:id', (req, res) => {
    let id = req.params.id;
    let length = data.length;
    data = data.filter(x => x.id != id);
    if (length != data.length) {
        res.statusCode = 204;
        res.statusMessage = 'Deleted!';
        let response = {
            code: res.statusCode,
            message: res.statusMessage
        };
        res.json(response);
    } else {
        res.statusCode = 404; //Bad request
        res.statusMessage = 'Product not found!';
        let error = {
            code: res.statusCode,
            message: res.statusMessage
        };
        res.json(error);
    }
});

module.exports = router;
























// var express = require('express');
// var router = express.Router();
// let mongoose = require('mongoose');

// const url = 'mongodb://localhost:27017';
// const dbName = 'myshopdb';

// mongoose.connect(`${url}/${dbName}`, {useNewUrlParser: true});

// var data = [
//     { id: 1, title: 'One', price: 1 },
//     { id: 2, title: 'Two', price: 2 },
//     { id: 3, title: 'Three', price: 3 },
// ];

// // GET /products
// /**
//  * @swagger
//  * /products:
//  *    get:
//  *      description: Return all products    
//  */
// router.get('/', async (req, res) => {
   
//     // var db = mongoose.connection;
//     // db.once('open', () => {
        
    

//     // });

//     res.json(result);
// });

// // GET /products/5
// router.get('/:id', async (req, res) => {
//     let id = req.params.id;
//     if (id) {
//         const client = new MongoClient(url, { useUnifiedTopology: true, useNewUrlParser: true });
//         let dbClient = await client.connect();
//         let db = dbClient.db(dbName);
//         let collection = db.collection('products');
//         let data = await collection.findOne({ _id: mongo.ObjectId(id) });
//         res.json(data);
//     } else {
//         res.statusCode = 404;
//         res.statusMessage = 'Product not found!';
//         let error = {
//             code: res.statusCode,
//             message: res.statusMessage
//         };
//         res.json(error);
//     }
// });

// // POST /products
// router.post('/', (req, res) => {
//     let item = req.body;
//     if (item.title && item.price) {
//         item.id = data[data.length - 1].id + 1;
//         data.push(item);
//         res.json(item); 
//     } else {
//         res.statusCode = 400; //Bad request
//         res.statusMessage = 'Bad request!';
//         let error = {
//             code: res.statusCode,
//             message: res.statusMessage
//         };
//         res.json(error);
//     }
// });

// // PUT /products
// router.put('/', (req, res) => {
//     let item = req.body;
//     let index = data.findIndex(x => x.id == item.id);
//     if (index != -1) {
//         data[index] = item;
//         res.json(item);        
//     } else {
//         res.statusCode = 404; //Bad request
//         res.statusMessage = 'Product not found!';
//         let error = {
//             code: res.statusCode,
//             message: res.statusMessage
//         };
//         res.json(error);
//     }
// });

// // DELETE /product/5
// router.delete('/:id', (req, res) => {
//     let id = req.params.id;
//     let length = data.length;
//     data = data.filter(x => x.id != id);
//     if (length != data.length) {
//         res.statusCode = 204;
//         res.statusMessage = 'Deleted!';
//         let response = {
//             code: res.statusCode,
//             message: res.statusMessage
//         };
//         res.json(response);
//     } else {
//         res.statusCode = 404; //Bad request
//         res.statusMessage = 'Product not found!';
//         let error = {
//             code: res.statusCode,
//             message: res.statusMessage
//         };
//         res.json(error);
//     }
// });

// module.exports = router;





















