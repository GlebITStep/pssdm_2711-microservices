# Links

[RabbitMQ](https://www.rabbitmq.com/)

[RabbitMQ Send](https://www.c-sharpcorner.com/article/publishing-rabbitmq-message-in-asp-net-core/)

[RabbitMQ Recieve](https://www.c-sharpcorner.com/article/consuming-rabbitmq-messages-in-asp-net-core/)

[IdentityServer](http://docs.identityserver.io/en/latest/)

[IdentityServer ASP.NET Identity](http://docs.identityserver.io/en/latest/quickstarts/8_aspnet_identity.html)

[Ocelot](https://ocelot.readthedocs.io/en/latest/introduction/gettingstarted.html)

[Ocelot Auth](https://ocelot.readthedocs.io/en/latest/features/authentication.html)

[eShopOnContainers](https://github.com/dotnet-architecture/eShopOnContainers)

[Docker](https://www.docker.com/)

[Kubernetes](https://kubernetes.io/)

[Azure DevOps](https://azure.microsoft.com/en-us/services/devops/)

[Microsoft Books](https://dotnet.microsoft.com/learn/aspnet/microservices-architecture)